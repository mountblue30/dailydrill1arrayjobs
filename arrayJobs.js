// 1. Find all Web Developers
// 2. Convert all the salary values into proper numbers instead of strings 
// 3. Assume that each salary amount is a factor of 10000 and correct it but add it as a new key (corrected_salary or something)
// 4. Find the sum of all salaries 
// 5. Find the sum of all salaries based on country using only HOF method
// 6. Find the average salary of based on country using only HOF method


// Importing dataset
let profiles = require("./dataset")


function findWebDevelopers(profiles) {
    if (Array.isArray(profiles) && profiles.length !== 0) {
        return profiles.filter((profile) => profile.job.includes("Web Developer"));
    } else {
        return [];
    }
}



function convertSalaryToNumber(profiles) {
    if (Array.isArray(profiles) && profiles.length !== 0) {
        profiles.forEach((profile) => {
            profile.salary = Number(profile.salary.slice(1));
        })
        return profiles;
    } else {
        return [];
    }
}


function correctedSalary(profiles) {
    if (Array.isArray(profiles) && profiles.length !== 0) {
        return profiles.map(element => {
            element["correctedSalary"] = +(Number(element.salary.slice(1)) * 10000).toFixed(2);
            return element;
        });
    } else {
        return [];
    }

}

function sumOfSalaries(profiles) {
    if (Array.isArray(profiles) && profiles.length !== 0) {
        return profiles.reduce((acc, curr) => {
            return +(acc + Number(curr.salary.slice(1))).toFixed(2)
        }, 0)
    } else {
        return [];
    }
}


function salaryBasedOnCountry(profiles, country) {
    if (Array.isArray(profiles) && profiles.length !== 0 && (typeof country === "string" || country.constructor === "String")) {
        return profiles.filter((profile) => profile.location == country).reduce((acc, curr) => {
            return +(acc + Number(curr.salary.slice(1))).toFixed(2);
        }, 0)
    } else {
        return [];
    }

}


function averageSalaryBasedOnCountry(profiles, country) {
    if (Array.isArray(profiles) && profiles.length !== 0 && (typeof country === "string" || country.constructor === "String")) {
        let filteredProfiles = profiles.filter((profile) => profile.location == country);
        let totalEmployees = filteredProfiles.length;
        let totalSalary = filteredProfiles.reduce((acc, curr) => {
            return +(acc + Number(curr.salary.slice(1)));
        }, 0)
        return +(totalSalary / totalEmployees).toFixed(2);
    } else {
        return [];
    }
}
